##################
Hb Tryton devtools
##################

Install
#######

.. code-block::

   pip install hb_tryton_devtools

Usage
#####

One console script exists **hb-tryton-admin**

.. code-block::

    hb-tryton-admin *command name* [*options*]


Existing commands:
------------------

To list the avaible command you can

.. code-block::

    hb-tryton-admin -h
    hb-tryton-admin *command* -h

Commands :

* interpreter : Interactive console, work with *Python* and *IPython*
* migrate : Do the creation or the migration of the main project
* script : Execute a python script with with tryton pool
* export-lang : Export *lang*.po or *module*.pot
* export-model : Export model ids as XML file

Commands to add:
----------------

* serve : serve tryton with update mode, if python or xml or cfg changed


Plugins
#######

It's possible to add new command by entry points.

In the setup add:

.. code-block::

    setup(
        entry_points={
            'hb.tryton.devtools.command': [
                '*command name*=*module path*:*CommandClass*',
                ...
            ]
        }
    )

In the module path:

.. code-block::

    from hb.tryton.devtools import CommandBase

    class *CommandClass*(CommandBase):

        def run(self):
            # do the import tryton here if need

            # all the server side Models, Methods is available
            Model = self.pool.get(*model's name*)

            # we are in a transaction
            with self.transaction.set_context(foo='bar'):
                ...
            don't forget to commit because transaction will be rollbacked
            self.transaction.commit()

Migration file
##############

The migration file must be named **migrate.py**, and the command 
```hb-tryton-migrate migrate``` have to be executed at the same path
as the migration file.

.. code-block::

    def migrate(session):
        previous_version = session.get_previous_version_for('module_name')
        modules_to_install_or_update = ['module_name']

        if previous_version is None:
            modules_to_install_or_update.append('ir')

        session.migrate(*modules_to_install_or_update)

        if previous_version is None:
            pass  # TODO

Helper
------

To help to start a project some helper is added to be used directely in the 
migration file under a transaction


.. code-block::

    from hb.tryton.devtools.helper import currencies, countries, accounts

    def init_project(session):
        with session.transaction():
            currencies.import_currencies()
            countries.import_countries()
            party = session.pool.get('party.party')(name="test")
            currency = session.pool.get('currency.currency').search(
                [('code', '=', 'EUR')])[0]
            company = session.pool.get('company.company')(
                currency=currency, party=party)
            company.save()
            accounts.fiscal_year(company=company)
            accounts.import_accounts_fr(company.id)


    def migrate(session):
        previous_version = session.get_previous_version_for('module_name')
        modules_to_install_or_update = ['module_name']

        if previous_version is None:
            modules_to_install_or_update.append('ir')

        session.migrate(*modules_to_install_or_update)

        if previous_version is None:
            init_project(session)

Testing
#######

This package add fixtures and helpper 

Import fixture in the **conftest.py**

.. code-block::

    import pytest  # noqa
    from hb.tryton.devtools.tests.testing  import *  # noqa

Do a simple test.

.. code-block::

    import pytest  # noqa


    class TestUser:

        def test_something(self, rollbacked_transaction, pool): 
            pass

Load the backport unittest from tryton

.. code-block::

    import pytest  # noqa
    from hb.tryton.devtools.tests.testing import MixinTestModule


    class TestModule(MixinTestModule):
        module = 'hb_devtools'  # module name

Do a test on specific user, by default the user is admin

.. code-block::

    import pytest  # noqa
    from hb.tryton.devtools.tests.testing import set_user


    class TestUser:

        def test_something(self, rollbacked_transaction, pool): 
            with set_user(login='a user'):
                pass


Some tools as backported from the tools of tryton without proteus:

* get_currency
* get_company
* create_user
* create_chart
* get_account
* create_recurrence_rule_set
* create_product_category
* create_parties
* get_statement_journal
* create_subscription_service
* create_subscription
* wizard_subscription_create_consumption_lines
* wizard_subscription_create_invoices_from_consumption

.. code-block::

    import pytest  # noqa
    from hb.tryton.devtools.tests.testing import (
        get_company, create_user, set_user)


    class TestUser:

        def test_something(self, rollbacked_transaction, pool): 
            company = get_company()
            user = create_user(company=company, group_names=['Sales'])
            with set_user(login=user.login):
                pass

Environ variable
################

For commands
------------

All
~~~

* TRYTON_CONFIG
* TRYTON_DATABASE_NAME
* TRYTON_DATABASE_URI

migrate
~~~~~~~

* TRYTON_LANG_CODE
* TRYTON_ADMIN_PASSWORD

For hb.tryton.devtools unittests
--------------------------------

* TEST_POSTGRES_DATABASE_URI

For tryton module unittests
---------------------------

* TRYTON_CONFIG
* TRYTON_DATABASE_NAME
* TRYTON_DATABASE_URI
