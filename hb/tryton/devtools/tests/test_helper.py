import pytest  # noqa
from datetime import date
from dateutil.relativedelta import relativedelta
from trytond.config import config
from trytond.pool import Pool
from hb.tryton.devtools.migrate.session import Session
from hb.tryton.devtools.helper import currencies, countries, accounts, user
from hb.tryton.devtools.tests.testing import get_company


@pytest.fixture(scope="class")
def session(request, database):

    class Options:
        database_name = ':memory:'

    class Migrate:
        options = Options()
        config = config
        pool = Pool(':memory:')

    return Session(Migrate())


class TestHelper:

    def test_set_user(self, session):
        session.migrate('res')
        with session.transaction() as transaction:
            assert transaction.user == 0
            with user.set_user(login='admin'):
                assert transaction.user == 1

            assert transaction.user == 0

    def test_currencies(self, session):
        session.migrate('currency')
        with session.transaction():
            currencies.import_currencies()

    def test_countries(self, session):
        session.migrate('country')
        with session.transaction():
            countries.import_countries()

    def test_account_fr(self, session):
        session.migrate('account_fr')
        with session.transaction():
            company = get_company()
            accounts.import_accounts_fr(company.id)


class TestHelperFiscalYear:

    def test_current_year(self, session):
        session.migrate('account')
        with session.transaction():
            company = get_company()
            fy = accounts.fiscal_year(company=company)
            today = date.today()
            assert fy.start_date.year == today.year
            assert fy.start_date.month == 1
            assert fy.start_date.day == 1
            assert fy.name == str(today.year)
            assert fy.end_date.year == today.year
            assert fy.end_date.month == 12
            assert fy.end_date.day == 31

    def test_current_year_account_invoice(self, session):
        session.migrate('account_invoice')
        with session.transaction():
            company = get_company()
            fy = accounts.fiscal_year(company=company)
            assert len(fy.invoice_sequences) == 1
            assert (
                fy.invoice_sequences[0].in_credit_note_sequence !=
                fy.invoice_sequences[0].in_invoice_sequence
            )

    def test_current_year_account_invoice2(self, session):
        session.migrate('account_invoice')
        with session.transaction():
            company = get_company()
            fy = accounts.fiscal_year(company=company, invoice_sequence={})
            assert len(fy.invoice_sequences) == 1
            assert (
                fy.invoice_sequences[0].in_credit_note_sequence ==
                fy.invoice_sequences[0].in_invoice_sequence
            )

    def test_on_year_2020(self, session):
        session.migrate('account')
        with session.transaction():
            company = get_company()
            fy = accounts.fiscal_year(
                company=company, start_date=date(2020, 1, 1))
            assert fy.start_date.year == 2020
            assert fy.end_date.year == 2020
            assert fy.end_date.month == 12
            assert fy.end_date.day == 31
            assert fy.name == "2020"

    def test_monthly(self, session):
        session.migrate('account')
        with session.transaction():
            company = get_company()
            fy = accounts.fiscal_year(company=company, interval=1)
            assert fy.periods[0].start_date == fy.start_date
            assert fy.periods[0].end_date == fy.start_date + relativedelta(
                months=+1, days=-1)
            assert fy.periods[1].start_date == fy.start_date + relativedelta(
                months=+1)
            assert len(fy.periods) == 12

    def test_quarterly(self, session):
        session.migrate('account')
        with session.transaction():
            company = get_company()
            # interval=3 pour faire des périodes trimestrielles
            fy = accounts.fiscal_year(company=company, interval=3)
            assert fy.periods[0].start_date == fy.start_date
            assert fy.periods[0].end_date == fy.start_date + relativedelta(
                months=+3, days=-1)
            assert fy.periods[1].start_date == fy.start_date + relativedelta(
                months=+3)
            assert len(fy.periods) == 4

    def test_with_seq_description(self, session):
        session.migrate('account')
        with session.transaction():
            company = get_company()
            fy = accounts.fiscal_year(
                company=company, post_move_sequence={'prefix': 'prefix_'})
            assert fy.post_move_sequence.prefix == 'prefix_'
