import pytest  # noqa
from hb.tryton.devtools.model import ExportModel
from trytond.config import config
from trytond.pool import Pool
from trytond.transaction import Transaction


class ExportBase:

    def init(self, pool, transaction):
        return

    database_name = ':memory:'
    module = 'hb_tryton'
    model = 'res.user'
    ids = [1]
    level = 1
    for_migrate = False
    commit = False


class ExportAdmin(ExportBase):
    pass


class ExportAdminWith3Level(ExportBase):
    level = 5


class ExportAdminWithCommit(ExportBase):
    commit = True


class ExportAdminWithForMigrate(ExportBase):
    for_migrate = True


class ExportMultiUser(ExportBase):

    def init(self, pool, transaction):
        User = pool.get('res.user')
        IrModelData = pool.get('ir.model.data')

        group = IrModelData.get_id('res', 'group_admin')
        user = User(
            name='Name',
            login='login',
            active=True,
        )
        user.groups = [group]
        user.save()
        self.ids = [1, user.id]


PARAMS = [
    ExportAdmin,
    ExportAdminWith3Level,
    ExportAdminWithCommit,
    ExportAdminWithForMigrate,
    ExportMultiUser,
]


@pytest.fixture(params=PARAMS)
def options(request):
    return request.param()


class TestInterpreter:

    def test_script(self, database, options):

        pool = Pool(':memory:')
        ExportModel.initialize_pool(pool)
        with Transaction().start(':memory:', 0) as transaction:
            options.init(pool, transaction)
            export = ExportModel(pool, transaction, options, config)
            export.run()
