import os
import pytest  # noqa
from hb.tryton.devtools.script import Script
from trytond.config import config
from trytond.pool import Pool
from trytond.transaction import Transaction


class TestInterpreter:

    def test_script(self, database):
        file_path = os.path.join(
            os.path.dirname(__file__),
            'script',
        )

        class Options:

            def __init__(self):
                self.database_name = ':memory:'
                self.python_script = file_path

        pool = Pool(':memory:')
        Script.initialize_pool(pool)
        with Transaction().start(':memory:', 0) as transaction:
            interpreter = Script(pool, transaction, Options(), config)
            interpreter.run()
