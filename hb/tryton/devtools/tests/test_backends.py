import os
import pytest  # noqa
from importlib import reload
from trytond.config import config
from trytond import backend
from hb.tryton.devtools.migrate.postgresql import PostgresBackend


class TestBackend:

    def test_sqlite(self, database):
        assert database.exist()

    def test_postgresql(self):
        uri = os.getenv('TEST_POSTGRES_DATABASE_URI', 'postgresql:///')
        old_uri = config.get('database', 'uri')
        try:
            config.set('database', 'uri', uri)
            reload(backend)
            database = PostgresBackend(dbname='devtools', config=config)
            database.create()
            database.initialize_tryton_database()
            assert database.exist()
        finally:
            config.set('database', 'uri', old_uri)
            reload(backend)
