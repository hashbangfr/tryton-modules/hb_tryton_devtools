import os
import pytest  # noqa
from importlib import reload
from trytond.config import config
from trytond.pool import Pool
from trytond import backend
from hb.tryton.devtools.migrate.session import Session
from trytond import __version__

HB_DEVTOOLS_VERSION = '0.0.1'


@pytest.fixture(scope="class")
def session(request, database):

    class Options:
        database_name = ':memory:'

    class Migrate:
        options = Options()
        config = config
        pool = Pool(':memory:')

    return Session(Migrate())


@pytest.fixture()
def empty_table(request, session):
    from trytond.transaction import Transaction

    with Transaction().start(session.options.database_name, 0) as transaction:
        with transaction.connection.cursor() as cursor:
            cursor.execute('DELETE FROM hb_tryton_modules_version;')
            cursor.execute("UPDATE ir_module set state='not activated';")


class TestSession:

    def test_session_with_postgres(self):

        class Options:
            database_name = 'devtools'

        class Migrate:
            options = Options()
            config = config
            pool = Pool('devtools')

        uri = os.getenv('TEST_POSTGRES_DATABASE_URI', 'postgresql:///')
        old_uri = config.get('database', 'uri')
        try:
            config.set('database', 'uri', uri)
            reload(backend)
            Session(Migrate())
        finally:
            config.set('database', 'uri', old_uri)
            reload(backend)

    def test_session_with_sqlite(self, session):
        assert str(session)

    def test_session_init(self, session):
        session.init = True
        session.migrate('res')
        with session.transaction():
            user = session.pool.get('res.user')(1)
            assert user.login == 'admin'
            assert user.language.code == 'en'

    def test_set_user(self, session):
        with session.transaction() as transaction:
            assert transaction.user == 0
            with session.set_user(login='admin'):
                assert transaction.user == 1

            assert transaction.user == 0

    def test_get_current_version(self, session):
        version = session.get_current_version_for('res')
        assert version == __version__

    def test_get_current_version_2(self, session):
        version = session.get_current_version_for('hb_devtools')
        assert version == HB_DEVTOOLS_VERSION

    def test_get_previous_version(self, session, empty_table):
        version = session.get_previous_version_for('hb_devtools')
        assert version is None

    def test_get_previous_version_update_version_uninstalled(
        self, session, empty_table
    ):
        session.migrate('res')
        session.update_previous_version_for_the_next_migration()
        version = session.get_previous_version_for('hb_devtools')
        assert version is None

    def test_get_previous_version_update_version_installed(
        self, session, empty_table
    ):
        session.migrate('hb_devtools')
        session.update_previous_version_for_the_next_migration()
        version = session.get_previous_version_for('hb_devtools')
        assert version == HB_DEVTOOLS_VERSION

    def test_import_file(self, session):
        session.migrate('hb_devtools')
        with session.transaction():
            session.import_xml_file('hb_devtools', 'data/user.xml')
            user, = session.pool.get('res.user').search([('name', '=', 'foo')])
            assert user.login == 'bar'
            assert user.language.code == 'en'
