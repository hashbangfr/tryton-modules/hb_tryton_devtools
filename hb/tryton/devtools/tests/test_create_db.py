import pytest  # noqa
from hb.tryton.devtools.migrate import CreateDB
from trytond.config import config
from trytond.pool import Pool


class OptionsBase:
    database_name = ':memory:'
    modules = ['ir']


class OptionsMulti(OptionsBase):
    modules = ['ir', 'res']


class OptionsDependencies(OptionsBase):
    modules = ['hb_devtools']


PARAMS = [
    OptionsBase,
    OptionsMulti,
    OptionsDependencies,
]


@pytest.fixture(params=PARAMS)
def options(request):
    return request.param()


class TestCreateDB:

    def test_initialize_pool(self):
        CreateDB.initialize_pool(Pool(':memory:'))

    def test_create_db(self, options):
        pool = Pool(':memory:')
        return CreateDB(pool, None, options, config).run()
