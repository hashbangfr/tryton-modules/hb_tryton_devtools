import pytest
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.config import config
from hb.tryton.devtools.migrate.sqlite import SQLilteBackend


@pytest.fixture(scope="session")
def database():
    lang = 'en'
    name = ':memory:'

    database = SQLilteBackend(dbname=name, config=config)
    if not database.exist():
        db = database.create()

        connection = db.get_connection()
        try:
            with connection.cursor():
                db.init()
            connection.commit()
        finally:
            db.put_connection(connection)

        pool = Pool(name)
        pool.init(update=['res', 'ir'], lang=[lang])
        with Transaction().start(name, 0):
            User = pool.get('res.user')
            Lang = pool.get('ir.lang')
            language, = Lang.search([('code', '=', lang)])
            language.translatable = True
            language.save()
            users = User.search([('login', '!=', 'root')])
            User.write(users, {'language': language.id})
            Module = pool.get('ir.module')
            Module.update_list()

    return database
