import pytest
import sys
from unittest.mock import patch
from hb.tryton.devtools.base import CommandBase
from hb.tryton import devtools
from hb.tryton.devtools import HbTrytonAdmin
from hb.tryton.devtools.base import define_parser_logging_arguments


class UnderTransaction(CommandBase):

    def run(self):
        assert self.pool
        assert self.transaction
        assert self.options
        assert self.config


class WithoutTransaction(CommandBase):

    WITH_TRANSACTION = False

    def run(self):
        assert self.pool
        assert self.transaction is None
        assert self.options
        assert self.config


class WithoutLogging(CommandBase):

    @classmethod
    def define_parser_arguments(self, parser):
        super().define_parser_arguments(parser)
        define_parser_logging_arguments(parser)

    def run(self):
        pass


@pytest.fixture(params=[UnderTransaction, WithoutTransaction, WithoutLogging])
def command(request):
    return request.param


class TestConsoleScript:

    def test_console_script(self, database, command):
        testargs = ["hb-tryton-admin", "test", "-d", ":memory:"]
        with patch.object(
            devtools,
            'get_commands',
            lambda: {'test': command}
        ):
            with patch.object(sys, 'argv', testargs):
                HbTrytonAdmin()
