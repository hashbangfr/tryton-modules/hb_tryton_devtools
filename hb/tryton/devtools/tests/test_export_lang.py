import os
import pytest  # noqa
from hb.tryton.devtools.lang import ExportLang
from trytond.config import config
from trytond.pool import Pool
from trytond.transaction import Transaction


LANGS = [
    'bg',
    'ca',
    'cs',
    'de',
    'es',
    'es_419',
    'et',
    'fa',
    'fi',
    'hu',
    'id',
    'it',
    'lo',
    'lt',
    'nl',
    'pl',
    'pt',
    'ro',
    'ru',
    'sl',
    'tr',
    'zh_CN',
    'en',
    'fr',
]


@pytest.fixture(params=LANGS)
def lang(request):
    return request.param


class TestExportLang:

    def test_export_pot(self, database):
        class Options:

            def __init__(self):
                self.database_name = ':memory:'
                self.module = 'ir'
                self.lang = None

        if os.path.isfile('ir.pot'):
            os.remove('ir.pot')

        pool = Pool(':memory:')
        ExportLang.initialize_pool(pool)
        with Transaction().start(':memory:', 0) as transaction:
            lang = ExportLang(pool, transaction, Options(), config)
            lang.run()

        assert os.path.isfile('ir.pot')

    def test_export_lang(self, database, lang):
        class Options:

            def __init__(self):
                self.database_name = ':memory:'
                self.module = 'ir'
                self.lang = lang

        lang_file = f'{lang}.po'
        if os.path.isfile(lang_file):
            os.remove(lang_file)

        pool = Pool(':memory:')
        with Transaction().start(pool.database_name, 0, readonly=True):
            pool.init(update=['ir'], lang=[lang])

        with Transaction().start(':memory:', 0) as transaction:
            Lang = pool.get('ir.lang')
            lang_, = Lang.search([('code', '=', lang)])
            lang_.translatable = True
            lang_.save()
            lang = ExportLang(pool, transaction, Options(), config)
            lang.run()

        assert os.path.isfile(lang_file)

    def test_export_unknown_lang(self, database):
        class Options:

            def __init__(self):
                self.database_name = ':memory:'
                self.module = 'ir'
                self.lang = 'UNKNONW'

        pool = Pool(':memory:')
        ExportLang.initialize_pool(pool)
        with Transaction().start(':memory:', 0) as transaction:
            lang = ExportLang(pool, transaction, Options(), config)
            lang.run()

        assert not os.path.isfile('UNKNONW.po')
