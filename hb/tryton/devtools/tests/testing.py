import pytest
import os
import operator
import inspect
from itertools import chain
from importlib import reload
from lxml import etree
from functools import reduce
from configparser import ConfigParser
from datetime import date
from decimal import Decimal
from hb.tryton.devtools.helper.user import set_user  # noqa


@pytest.fixture(scope='session')
def dbname(request):
    dbname = os.getenv('TRYTON_DATABASE_NAME', 'test')
    return dbname


@pytest.fixture(scope='session')
def config(request, dbname):
    from trytond.config import config
    from trytond import backend

    configfile = os.getenv('TRYTON_CONFIG', 'postgresql:///')
    if configfile:
        config.update_etc(configfile)

    uri = os.getenv('TRYTON_DATABASE_URI', 'postgresql:///')
    config.set('database', 'uri', uri)
    reload(backend)
    return config


@pytest.fixture(scope='session')
def pool(request, config, dbname):
    from trytond.pool import Pool
    from trytond.transaction import Transaction
    pool_ = Pool(dbname)
    with Transaction().start(dbname, 0, readonly=True):
        pool_.init()

    return pool_


@pytest.fixture()
def rollbacked_transaction(request, config, pool, dbname):
    from trytond.transaction import Transaction
    from trytond.cache import Cache

    with Transaction().start(dbname, 0, readonly=True):
        User = pool.get('res.user')
        user = User.search([('login', '=', 'admin')])[0]
        context = User._get_preferences(user, context_only=True)

    transaction = Transaction()
    transaction.start(pool.database_name, user.id, context=context)

    def rollback():
        transaction.rollback()
        Cache.drop(pool.database_name)
        transaction.stop()

    request.addfinalizer(rollback)
    return transaction


def run_tasks(rollbacked_transaction):
    from trytond.status import processing
    from trytond.pool import Pool

    pool = Pool()

    Queue = pool.get('ir.queue')
    while rollbacked_transaction.tasks:
        task_id = rollbacked_transaction.tasks.pop()
        name = '<Task %s@%s>' % (task_id, pool.database_name)
        tasks = Queue.search([('id', '=', task_id)])
        if tasks:
            with processing(name):
                tasks[0].run()


def get_currency(code='USD', rate='1.0'):
    # from trytond.modules.currency.tests.tools
    from trytond.pool import Pool

    pool = Pool()
    Currency = pool.get('currency.currency')

    currencies = Currency.search([('code', '=', code)])
    if not currencies:
        currency = Currency(
            name=code, symbol=code, code=code, rounding=Decimal('0.01'))
        currency.save()
        if rate is not None:
            CurrencyRate = pool.get('currency.currency.rate')
            rate = Decimal(rate)
            CurrencyRate(date=date.min, rate=rate, currency=currency).save()
    else:
        currency, = currencies
    return currency


def wizard(model):
    from trytond.pool import Pool

    pool = Pool()
    Wizard = pool.get(model, type='wizard')
    session, start, end = Wizard.create()
    return Wizard(session)


def create_parties():
    from trytond.pool import Pool

    pool = Pool()
    Party = pool.get('party.party')
    parties = []
    for x in range(10):
        parties.append(Party(name='Party %d' % x))

    Party.save(parties)
    return parties


def create_user(login='test', company=None, group_names=None):
    from trytond.pool import Pool

    pool = Pool()
    User = pool.get('res.user')
    groups = []
    if group_names:
        Group = pool.get('res.group')
        if not isinstance(group_names, list):
            group_names = [group_names]

        groups.extend(Group.search([('name', 'in', group_names)]))

    user = User(name=login, login=login, groups=groups)

    if company is not None:
        user.companies = [company]
        user.company = company

    user.save()

    return user


def create_chart(company=None, chart='account.account_template_root_en'):
    # from trytond.modules.account.tests.tools
    from trytond.pool import Pool

    pool = Pool()
    AccountTemplate = pool.get('account.account.template')
    ModelData = pool.get('ir.model.data')

    if not company:
        company = get_company()

    module, xml_id = chart.split('.')
    db_id = ModelData.get_id(module, xml_id)
    account_template = AccountTemplate.browse([db_id])[0]

    create_chart = wizard('account.create_chart')
    create_chart._execute('account')
    create_chart.account.account_template = account_template
    create_chart.account.company = company
    create_chart._execute('create_account')

    accounts = get_accounts(company)

    create_chart.properties.company = company
    if accounts['receivable'].party_required:
        create_chart.properties.account_receivable = accounts['receivable']
    if accounts['payable'].party_required:
        create_chart.properties.account_payable = accounts['payable']

    create_chart._execute('create_properties')
    return create_chart


def get_accounts(company=None):
    # from trytond.modules.account.tests.tools
    from trytond.pool import Pool

    pool = Pool()
    Account = pool.get('account.account')

    if not company:
        company = get_company()

    accounts = {}
    for type in ['receivable', 'payable', 'revenue', 'expense']:
        try:
            accounts[type], = Account.search([
                ('type.%s' % type, '=', True),
                ('company', '=', company.id),
            ], limit=1)
        except ValueError:
            pass

    try:
        accounts['cash'], = Account.search([
            ('company', '=', company.id),
            ('name', '=', 'Main Cash'),
        ], limit=1)
    except ValueError:
        pass

    try:
        accounts['tax'], = Account.search([
            ('company', '=', company.id),
            ('name', '=', 'Main Tax'),
        ], limit=1)
    except ValueError:
        pass

    return accounts


def get_statement_journal(company):
    from trytond.pool import Pool

    pool = Pool()
    AccountJournal = pool.get('account.journal')
    StatementJournal = pool.get('account.statement.journal')
    account_journal, = AccountJournal.search([('code', '=', 'STA')], limit=1)

    accounts = get_accounts(company)
    cash = accounts['cash']

    journal_number = StatementJournal(
        name="Number",
        journal=account_journal,
        account=cash,
        validation='number_of_lines',
        company=company,
    )
    journal_number.save()
    return journal_number


def get_company(party=None, currency=None):
    # from trytond.modules.company.tests.tools
    from trytond.pool import Pool

    pool = Pool()
    Party = pool.get('party.party')

    company_config = wizard('company.company.config')
    company_config._execute('company')
    company = company_config.company

    if party is None:
        party = Party(name='Dunder mifflin')
        party.save()

    company.party = party

    if currency is None:
        currency = get_currency()

    company.currency = currency

    company_config._execute('add')
    return company


def create_recurrence_rule_set(**kwargs):
    from trytond.pool import Pool

    pool = Pool()
    RecurrenceRuleSet = pool.get('sale.subscription.recurrence.rule.set')
    RecurrenceRule = pool.get('sale.subscription.recurrence.rule')
    freq = kwargs.get('freq', 'monthly')
    name = kwargs.get('name', freq.capitalize())

    rule = RecurrenceRuleSet(
        name=name,
        rules=[
            RecurrenceRule(
                freq=freq,
                interval=kwargs.get('interval', 1),
            )
        ]
    )
    rule.save()
    return rule


def create_product_category(company, get_accounts=get_accounts):
    from trytond.pool import Pool

    pool = Pool()
    ProductCategory = pool.get('product.category')
    account_category = ProductCategory(name="Account Category")
    account_category.accounting = True
    accounts = get_accounts(company)
    revenue = accounts['revenue']
    account_category.account_revenue = revenue
    account_category.save()
    return account_category


def create_subscription_service(
    name='Rental', uom_name='Unit', template_type='service',
    account_category=None, recurrence=None
):
    from trytond.pool import Pool

    pool = Pool()
    Service = pool.get('sale.subscription.service')
    ProductTemplate = pool.get('product.template')
    Product = pool.get('product.product')
    Uom = pool.get('product.uom')

    unit, = Uom.search([('name', '=', uom_name)])

    template = ProductTemplate(
        name=name,
        default_uom=unit,
        type=template_type,
        list_price=Decimal('10'),
        account_category=account_category,
    )
    template.on_change_type()
    template.on_change_default_uom()
    template.save()

    product = Product(template=template)
    product.on_change_template()
    product.save()

    service = Service(
        product=product,
        consumption_recurrence=recurrence,
    )
    service.save()
    return service


def create_subscription(
    customer=None, recurrence=None, service=None, quantity=10,
):
    from trytond.pool import Pool

    pool = Pool()
    Subscription = pool.get('sale.subscription')
    SubscriptionLine = pool.get('sale.subscription.line')

    line = SubscriptionLine(
        service=service,
        quantity=quantity,
        start_date=date(2016, 1, 1),
    )
    line.on_change_service()
    subscription = Subscription(
        party=customer,
        start_date=date(2016, 1, 1),
        invoice_start_date=date(2016, 2, 1),
        invoice_recurrence=recurrence,
        lines=[line]
    )
    subscription.on_change_party()
    subscription.save()

    Subscription.quote([subscription])
    Subscription.run([subscription])
    return subscription


def wizard_subscription_create_consumption_lines(at=None):
    if at is None:
        at = date(2016, 1, 31)

    line_consumption_create = wizard(
        'sale.subscription.line.consumption.create')
    line_consumption_create.start.date = at
    line_consumption_create._execute('create_')


def wizard_subscription_create_invoices_from_consumption(at=None):
    if at is None:
        at = date(2016, 2, 1)

    create_invoice = wizard('sale.subscription.create_invoice')
    create_invoice.start.date = at
    create_invoice._execute('create_')


class MixinTestModule:
    'Trytond Test Case'
    module = None
    extras = None
    language = 'en'

    def test_rec_name(self, rollbacked_transaction, pool):
        from trytond.pool import isregisteredby

        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            # Skip testing default value even if the field doesn't exist
            # as there is a fallback to id
            if model._rec_name == 'name':
                continue
            assert (
                model._rec_name in model._fields.keys()
            ), 'Wrong _rec_name "%s" for %s' % (model._rec_name, mname)
            field = model._fields[model._rec_name]
            assert field._type in {'char', 'text'}, (
                "Wrong '%s' type for _rec_name of %s'" % (field._type, mname))

    def test_model__access__(self, rollbacked_transaction, pool):
        "Test existing model __access__"
        from trytond.pool import isregisteredby

        for mname, Model in pool.iterobject():
            if not isregisteredby(Model, self.module):
                continue
            for field_name in Model.__access__:
                assert field_name in Model._fields.keys(), (
                    "Wrong __access__ '%s' for %s" % (field_name, mname))
                field = Model._fields[field_name]
                Target = field.get_target()
                assert Target, 'Missing target for __access__ "%s" of %s' % (
                    field_name, mname)

    def test_view(self, rollbacked_transaction, pool):
        'Test validity of all views of the module'
        View = pool.get('ir.ui.view')
        views = View.search([
                ('module', '=', self.module),
                ('model', '!=', ''),
                ])
        for view in views:
            if not view.inherit or view.inherit.model == view.model:
                assert view.arch, (
                    'missing architecture for view "%(name)s" '
                    'of model "%(model)s"' % {
                        'name': view.name or str(view.id),
                        'model': view.model,
                    })

            if view.inherit and view.inherit.model == view.model:
                view_id = view.inherit.id
            else:
                view_id = view.id
            model = view.model
            Model = pool.get(model)
            res = Model.fields_view_get(view_id)
            assert res['model'] == model
            tree = etree.fromstring(res['arch'])

            validator = etree.RelaxNG(etree=View.get_rng(res['type']))
            validator.assertValid(tree)

            tree_root = tree.getroottree().getroot()

            for element in tree_root.iter():
                if element.tag in ('field', 'label', 'separator', 'group'):
                    for attr in ['name', 'icon', 'symbol']:
                        field = element.get(attr)
                        if field:
                            assert field in res['fields'].keys(), (
                                'Missing field: %s in %s' % (
                                    field, Model.__name__))
                if element.tag == 'button':
                    button_name = element.get('name')
                    assert button_name in Model._buttons.keys(), (
                        "Button '%s' is not in %s._buttons"
                        % (button_name, Model.__name__))

    def test_icon(self, rollbacked_transaction, pool):
        "Test icons of the module"
        Icon = pool.get('ir.ui.icon')
        icons = Icon.search([('module', '=', self.module)])
        for icon in icons:
            assert icon.icon

    def test_rpc_callable(self, rollbacked_transaction, pool):
        'Test that RPC methods are callable'
        for _, model in pool.iterobject():
            for method_name in model.__rpc__:
                assert callable(getattr(model, method_name, None)), (
                    "'%s' is not callable on '%s'" % (
                        method_name, model.__name__))

    def test_missing_depends(self, rollbacked_transaction, pool):
        'Test for missing depends'
        from trytond.pool import isregisteredby
        from trytond.model import ModelView

        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            for fname, field in model._fields.items():
                fields = set(model._fields)
                depends = {
                    f for f in field.depends if not f.startswith('_parent_')}
                assert depends <= fields, (
                    'Missing depends %s in "%s"."%s"' % (
                        list(fields - depends), mname, fname))
                assert depends <= set(model._fields), (
                    'Unknown depends %s in "%s"."%s"' % (
                        list(depends - set(model._fields)), mname, fname))
            if issubclass(model, ModelView):
                for bname, button in model._buttons.items():
                    depends = set(button.get('depends', []))
                    assert depends <= set(model._fields), (
                        'Unknown depends %s in button "%s"."%s"' % (
                            list(depends - set(model._fields)), mname, bname))

    def test_depends(self, rollbacked_transaction, pool):  # noqa:C901
        "Test depends"
        from trytond.model import fields
        from trytond.pool import isregisteredby

        def test_missing_relation(depend, depends, qualname):
            prefix = []
            for d in depend.split('.'):
                if d.startswith('_parent_'):
                    relation = '.'.join(
                        prefix + [d[len('_parent_'):]])
                    assert relation in depends, 'Missing "%s" in %s' % (
                        relation, qualname)

                prefix.append(d)

        def test_parent_empty(depend, qualname):
            if depend.startswith('_parent_'):
                assert '.' in depend, 'Invalid empty "%s" in %s' % (
                    depend, qualname)

        def test_missing_parent(model, depend, depends, qualname):
            dfield = model._fields.get(depend)
            parent_depends = {d.split('.', 1)[0] for d in depends}
            if dfield and dfield._type == 'many2one':
                target = dfield.get_target()
                for tfield in target._fields.values():
                    if (tfield._type == 'one2many'
                            and tfield.model_name == mname
                            and tfield.field == depend):
                        assert '_parent_%s' % depend in parent_depends, (
                            'Missing "_parent_%s" in %s' % (depend, qualname))

        def test_depend_exists(model, depend, qualname):
            try:
                depend, nested = depend.split('.', 1)
            except ValueError:
                nested = None
            if depend.startswith('_parent_'):
                depend = depend[len('_parent_'):]
            assert isinstance(getattr(model, depend, None), fields.Field), (
                'Unknown "%s" in %s' % (depend, qualname))

            if nested:
                target = getattr(model, depend).get_target()
                test_depend_exists(target, nested, qualname)

        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            for fname, field in model._fields.items():
                for attribute in ['depends', 'on_change', 'on_change_with',
                                  'selection_change_with', 'autocomplete']:
                    depends = getattr(field, attribute, [])
                    qualname = '"%s"."%s"."%s"' % (mname, fname, attribute)
                    for depend in depends:
                        test_depend_exists(model, depend, qualname)
                        test_missing_relation(depend, depends, qualname)
                        test_parent_empty(depend, qualname)
                        if attribute != 'depends':
                            test_missing_parent(
                                model, depend, depends, qualname)

    def test_field_methods(self, rollbacked_transaction, pool):  # noqa:C901
        'Test field methods'
        from trytond.pool import isregisteredby
        from trytond.model import fields

        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue

            for attr in dir(model):
                for prefixes in [['default_'],
                                 ['on_change_', 'on_change_with_'],
                                 ['order_'], ['domain_'], ['autocomplete_']]:
                    if attr in (
                        'on_change_with',
                        'on_change_notify',
                        'default_get',
                        *model._fields,
                    ):
                        continue

                    if mname == 'ir.rule' and attr == 'domain_get':
                        continue

                    fnames = [attr[len(prefix):] for prefix in prefixes
                              if attr.startswith(prefix)]

                    if not fnames:
                        continue
                    assert any(f in model._fields for f in fnames), (
                        'Field method "%s"."%s" for unknown field' % (
                            mname, attr))

                    if attr.startswith('default_'):
                        fname = attr[len('default_'):]
                        if isinstance(model._fields[fname], fields.MultiValue):
                            try:
                                getattr(model, attr)(pattern=None)
                            # get_multivalue may raise an AttributeError
                            # if pattern is not defined on the model
                            except AttributeError:
                                pass
                        else:
                            getattr(model, attr)()
                    elif attr.startswith('order_'):
                        tables = {None: (model.__table__(), None)}
                        getattr(model, attr)(tables)
                    elif any(attr.startswith(p) for p in [
                                'on_change_',
                                'on_change_with_',
                                'autocomplete_']):
                        record = model()
                        getattr(record, attr)()

    def test_field_relation_target(self, rollbacked_transaction, pool):
        "Test field relation and target"
        from trytond.pool import isregisteredby
        from trytond.model import fields, ModelSQL

        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            for fname, field in model._fields.items():
                if isinstance(field, fields.One2Many):
                    Relation = field.get_target()
                    rfield = field.field
                elif isinstance(field, fields.Many2Many):
                    Relation = field.get_relation()
                    rfield = field.origin
                else:
                    continue
                if rfield:
                    assert rfield in Relation._fields.keys(), (
                        'Missing relation field "%s" on "%s" '
                        'for "%s"."%s"') % (
                            rfield, Relation.__name__, mname, fname)

                    reverse_field = Relation._fields[rfield]
                    assert reverse_field._type in [
                        'reference', 'many2one', 'one2one'
                    ], (
                        'Wrong type for relation field "%s" on "%s" '
                        'for "%s"."%s"') % (
                            rfield, Relation.__name__, mname, fname)

                    if (reverse_field._type == 'many2one'
                            and issubclass(model, ModelSQL)
                            # Do not test table_query models
                            # as they can manipulate their id
                            and not callable(model.table_query)):
                        assert (
                            reverse_field.model_name == model.__name__
                        ), (
                            'Wrong model for relation field "%s" on "%s" '
                            'for "%s"."%s"'
                        ) % (rfield, Relation.__name__, mname, fname)
                Target = field.get_target()
                assert Target, 'Missing target for "%s"."%s"' % (mname, fname)

    def test_menu_action(self, rollbacked_transaction, pool):
        'Test that menu actions are accessible to menu\'s group'
        Menu = pool.get('ir.ui.menu')
        ModelData = pool.get('ir.model.data')

        module_menus = ModelData.search([
                ('model', '=', 'ir.ui.menu'),
                ('module', '=', self.module),
                ])
        menus = Menu.browse([mm.db_id for mm in module_menus])
        for menu, module_menu in zip(menus, module_menus):
            if not menu.action_keywords:
                continue
            menu_groups = set(menu.groups)
            actions_groups = reduce(
                operator.or_,
                (set(k.action.groups) for k in menu.action_keywords
                 if k.keyword == 'tree_open'))
            if not actions_groups:
                continue
            assert menu_groups <= actions_groups, (
                'Menu "%(menu_xml_id)s" actions are not accessible to '
                '%(groups)s' % {
                    'menu_xml_id': module_menu.fs_id,
                    'groups': ','.join(g.name
                                       for g in menu_groups - actions_groups),
                })

    def test_model_access(self, rollbacked_transaction, pool):
        'Test missing default model access'
        Access = pool.get('ir.model.access')
        no_groups = {a.model.model
                     for a in Access.search([('group', '=', None)])}

        def has_access(Model, models):
            if Model.__name__ in models:
                return True
            for field_name in Model.__access__:
                Target = Model._fields[field_name].get_target()
                if has_access(Target, models):
                    return True

        for mname, Model in pool.iterobject():
            if has_access(Model, no_groups):
                no_groups.add(mname)

        with_groups = {a.model.model for a in Access.search([
                    ('group', '!=', None),
                    ])}

        assert no_groups >= with_groups, (
            'Model "%(models)s" are missing a default access' % {
                'models': list(with_groups - no_groups)})

    def test_workflow_transitions(self, rollbacked_transaction, pool):
        'Test all workflow transitions exist'
        from trytond.pool import isregisteredby
        from trytond.model import Workflow
        from trytond.tools import is_instance_method

        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            if not issubclass(model, Workflow):
                continue
            field = getattr(model, model._transition_state)
            if isinstance(field.selection, (tuple, list)):
                values = field.selection
            else:
                # instance method may not return all the possible values
                if is_instance_method(model, field.selection):
                    continue
                values = getattr(model, field.selection)()
            states = set(dict(values))
            transition_states = set(chain(*model._transitions))
            assert transition_states <= states, (
                'Unknown transition states "%(states)s" '
                'in model "%(model)s". ' % {
                    'states': list(transition_states - states),
                    'model': model.__name__})

    def test_wizards(self, rollbacked_transaction, pool):
        'Test wizards are correctly defined'
        from trytond.pool import isregisteredby
        from trytond.wizard import StateView, StateAction

        for wizard_name, wizard in pool.iterobject(type='wizard'):
            if not isregisteredby(wizard, self.module, type_='wizard'):
                continue

            session_id, start_state, _ = wizard.create()
            assert start_state in wizard.states.keys(), (
                'Unknown start state '
                '"%(state)s" on wizard "%(wizard)s"' % {
                    'state': start_state,
                    'wizard': wizard_name})

            wizard_instance = wizard(session_id)
            for state_name, state in wizard_instance.states.items():
                if isinstance(state, StateView):
                    # Don't test defaults as they may depend on context
                    view = state.get_view(wizard_instance, state_name)
                    assert view.get('type') == 'form', (
                        'Wrong view type for "%(state)s" '
                        'on wizard "%(wizard)s"' % {
                            'state': state_name,
                            'wizard': wizard_name})

                    for button in state.get_buttons(
                        wizard_instance, state_name
                    ):
                        if button['state'] == wizard.end_state:
                            continue

                        assert (
                            button['state'] in wizard_instance.states.keys()
                        ), (
                            'Unknown button state from "%(state)s" '
                            'on wizard "%(wizard)s' % {
                                'state': state_name,
                                'wizard': wizard_name})

                if isinstance(state, StateAction):
                    state.get_action()

    def test_selection_fields(self, rollbacked_transaction, pool):
        'Test selection values'
        from trytond.pool import isregisteredby
        from trytond.tools import is_instance_method

        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            for field_name, field in model._fields.items():
                selection = getattr(field, 'selection', None)
                if selection is None:
                    continue
                selection_values = field.selection
                if not isinstance(selection_values, (tuple, list)):
                    sel_func = getattr(model, field.selection)
                    if not is_instance_method(model, field.selection):
                        selection_values = sel_func()
                    else:
                        record = model()
                        selection_values = sel_func(record)
                assert all(len(v) == 2 for v in selection_values), (
                    'Invalid selection values "%(values)s" on field '
                    '"%(field)s" of model "%(model)s"' % {
                        'values': selection_values,
                        'field': field_name,
                        'model': model.__name__})

    def test_function_fields(self, rollbacked_transaction, pool):
        "Test function fields methods"
        from trytond.pool import isregisteredby
        from trytond.model.fields import Function

        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            for field_name, field in model._fields.items():
                if not isinstance(field, Function):
                    continue
                for func_name in [field.getter, field.setter, field.searcher]:
                    if not func_name:
                        continue
                    assert getattr(model, func_name, None), (
                        "Missing method '%(func_name)s' "
                        "on model '%(model)s' for field '%(field)s'" % {
                            'func_name': func_name,
                            'model': model.__name__,
                            'field': field_name})

    def test_ir_action_window(self, rollbacked_transaction, pool):
        'Test action windows are correctly defined'
        from trytond.pyson import PYSONDecoder
        from trytond.transaction import Transaction

        ModelData = pool.get('ir.model.data')
        ActionWindow = pool.get('ir.action.act_window')
        for model_data in ModelData.search([
                    ('module', '=', self.module),
                    ('model', '=', 'ir.action.act_window'),
                    ]):
            action_window = ActionWindow(model_data.db_id)
            if not action_window.res_model:
                continue
            Model = pool.get(action_window.res_model)
            for active_id, active_ids in [
                    (None, []),
                    (1, [1]),
                    (1, [1, 2]),
                    ]:
                decoder = PYSONDecoder({
                        'active_id': active_id,
                        'active_ids': active_ids,
                        'active_model': action_window.res_model,
                        })
                domain = decoder.decode(action_window.pyson_domain)
                order = decoder.decode(action_window.pyson_order)
                context = decoder.decode(action_window.pyson_context)
                search_value = decoder.decode(action_window.pyson_search_value)
                if action_window.context_domain:
                    domain = [
                        'AND', domain,
                        decoder.decode(action_window.context_domain)]
                with Transaction().set_context(context):
                    Model.search(
                        domain, order=order, limit=action_window.limit)
                    if search_value:
                        Model.search(search_value)
                for action_domain in action_window.act_window_domains:
                    if not action_domain.domain:
                        continue
                    Model.search(decoder.decode(action_domain.domain))
            if action_window.context_model:
                pool.get(action_window.context_model)

    def test_modelsingleton_inherit_order(self, rollbacked_transaction, pool):
        'Test ModelSingleton, ModelSQL, ModelStorage order in the MRO'
        from trytond.pool import isregisteredby
        from trytond.model import ModelSingleton, ModelSQL

        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            if (not issubclass(model, ModelSingleton)
                    or not issubclass(model, ModelSQL)):
                continue
            mro = inspect.getmro(model)
            singleton_index = mro.index(ModelSingleton)
            sql_index = mro.index(ModelSQL)
            assert singleton_index <= sql_index, (
                "ModelSingleton must appear before ModelSQL in the parent "
                "classes of '%s'." % mname)

    def test_buttons_registered(self, rollbacked_transaction, pool):
        'Test all buttons are registered in ir.model.button'
        from trytond.pool import isregisteredby
        from trytond.model import ModelView

        Button = pool.get('ir.model.button')
        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            if not issubclass(model, ModelView):
                continue
            ir_buttons = {
                b.name for b in Button.search([
                    ('model.model', '=', model.__name__)])}
            buttons = set(model._buttons)
            assert ir_buttons >= buttons, (
                'The buttons "%(buttons)s" of Model "%(model)s" are not '
                'registered in ir.model.button.' % {
                    'buttons': list(buttons - ir_buttons),
                    'model': model.__name__})

    def test_buttons_states(self, rollbacked_transaction, pool):
        "Test the states of buttons"
        from trytond.pool import isregisteredby
        from trytond.model import ModelView

        keys = {'readonly', 'invisible', 'icon', 'pre_validate', 'depends'}
        for mname, model in pool.iterobject():
            if not isregisteredby(model, self.module):
                continue
            if not issubclass(model, ModelView):
                continue
            for button, states in model._buttons.items():
                assert set(states).issubset(keys), (
                    'The button "%(button)s" of Model "%(model)s" has '
                    'extra keys "%(keys)s".' % {
                        'button': button,
                        'model': mname,
                        'keys': set(states) - keys})

    def test_xml_files(self, rollbacked_transaction, pool):
        "Test validity of the xml files of the module"
        from trytond.tools import file_open

        config = ConfigParser()
        with file_open(
            '%s/tryton.cfg' % self.module,
            subdir='modules', mode='r', encoding='utf-8'
        ) as fp:
            config.read_file(fp)
        if not config.has_option('tryton', 'xml'):
            return
        with file_open('tryton.rng', subdir='', mode='rb') as fp:
            rng = etree.parse(fp)
        validator = etree.RelaxNG(etree=rng)
        for xml_file in filter(None, config.get('tryton', 'xml').splitlines()):
            with file_open(
                '%s/%s' % (self.module, xml_file),
                subdir='modules', mode='rb'
            ) as fp:
                tree = etree.parse(fp)
            validator.assertValid(tree)
