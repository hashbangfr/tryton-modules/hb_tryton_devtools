def install_data(session):
    with session.transaction():
        pass


def migrate(session):
    previous_version = session.get_previous_version_for('res')
    modules_to_install_or_update = ['res']

    if previous_version is None:
        modules_to_install_or_update.append('ir')

    session.migrate(*modules_to_install_or_update)

    if previous_version is None:
        install_data(session)

    if previous_version is not None and previous_version < '0.1.0':
        pass
