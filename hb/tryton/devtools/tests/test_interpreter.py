import sys
import pytest
from io import StringIO
from unittest.mock import patch
from hb.tryton.devtools.interpreter import Interpreter
from trytond.config import config
from trytond.pool import Pool
from trytond.transaction import Transaction

try:
    import IPython  # noqa
except ImportError:
    IPython = None


def funct_interpreter():

    class Options:

        def __init__(self):
            self.database_name = ':memory:'
            self.python_script = None

    pool = Pool(':memory:')
    Interpreter.initialize_pool(pool)
    with Transaction().start(':memory:', 0) as transaction:
        interpreter = Interpreter(pool, transaction, Options(), config)
        with patch('sys.stdin', StringIO("EOF")):
            interpreter.run()


def interpreter_without_isatty():
    with patch.object(sys.stdin, 'isatty', lambda: False):
        funct_interpreter()


def interpreter_with_isatty():
    with patch.object(sys.stdin, 'isatty', lambda: True):
        funct_interpreter()


@pytest.fixture(params=[interpreter_with_isatty, interpreter_without_isatty])
def func(request):
    return request.param


class TestInterpreter:

    @pytest.mark.skipif(not IPython, reason="IPython is not installed")
    def test_interpreter(self, database):
        funct_interpreter()

    @pytest.mark.skipif(not IPython, reason="IPython is not installed")
    def test_interpreter_without_ipython_1(
        self, database, monkeypatch, func
    ):
        monkeypatch.delattr("IPython.embed")
        func()

    @pytest.mark.skipif(IPython, reason="IPython is installed")
    def test_interpreter_without_ipython_2(self, database, func):
        func()
