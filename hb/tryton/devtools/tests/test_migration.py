import os
import pytest  # noqa
from hb.tryton.devtools.migrate import Migrate
from trytond.config import config
from trytond.pool import Pool


@pytest.fixture(scope="class")
def migrate(request, database):

    class Options:

        def __init__(self):
            self.database_name = ':memory:'

    pool = Pool(':memory:')
    return Migrate(pool, None, Options(), config)


class TestMigration:

    def test_migration(self, migrate):
        try:
            current_path = os.path.abspath('.')
            test_path = os.path.dirname(os.path.abspath(__file__))
            os.chdir(test_path)
            migrate.run()
        finally:
            os.chdir(current_path)

    def test_initialize_pool(self):
        Migrate.initialize_pool(Pool(':memory:'))

    def test_missing_migration(self, migrate):
        with pytest.raises(ImportError):
            migrate.run()
