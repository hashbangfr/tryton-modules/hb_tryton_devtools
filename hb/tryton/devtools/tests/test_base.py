import pytest
from hb.tryton.devtools import get_commands, get_parser
from hb.tryton.devtools.base import CommandBase, define_parser_logging_arguments
from hb.tryton.devtools.interpreter import Interpreter
from hb.tryton.devtools.migrate import Migrate, CreateDB
from hb.tryton.devtools.lang import ExportLang
from hb.tryton.devtools.model import ExportModel


class Mine(CommandBase):
    "Explanation of the command"

    @classmethod
    def define_parser_arguments(self, parser):
        define_parser_logging_arguments(parser)


COMMANDS = [
    get_commands(),
    {'base': CommandBase},
    {'mine': Mine},
    {'interpreter': Interpreter},
    {'migrate': Migrate},
    {'create-db': CreateDB},
    {'export-lang': ExportLang},
    {'export-model': ExportModel},
]


@pytest.fixture(params=COMMANDS)
def commands(request):
    return request.param


class TestBase:

    def test_get_commands(self):
        commands_name = [x for x in get_commands().keys()]
        commands_name.sort()
        assert commands_name == [
            'create-db',
            'export-lang',
            'export-model',
            'interpreter',
            'migrate',
            'script',
        ]

    def test_get_parser(self, commands):
        assert get_parser(commands)
