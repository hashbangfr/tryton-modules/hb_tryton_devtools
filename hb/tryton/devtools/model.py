from .base import CommandBase, define_parser_logging_arguments
from lxml import etree
from logging import getLogger


logger = getLogger(__name__)


class ExportModel(CommandBase):
    """Model xml"""

    TRYTON_FIELDS = [
        'create_date',
        'create_uid',
        'write_uid',
        'write_date',
        'id',
        'rec_name',
    ]

    @classmethod
    def define_parser_arguments(self, parser):
        super().define_parser_arguments(parser)
        define_parser_logging_arguments(parser)
        parser.add_argument(
            '--for-migrate',
            dest="for_migrate",
            action='store_true',
            help='Specify the level of entries to export')
        parser.add_argument(
            '--commit',
            dest="commit",
            action='store_true',
            help='Specify if the ir.model.data must be committed')
        parser.add_argument(
            '--level',
            dest="level",
            default=1,
            type=int,
            help='Specify the level of entries to export')
        parser.add_argument(
            dest="module",
            help='Specify the name of the module to export')
        parser.add_argument(
            dest="model",
            help='Specify the name of the model to export')
        parser.add_argument(
            dest="ids",
            nargs='+',
            type=int,
            help='Specify the id on the model to export')

    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.objects_to_export = []
        self.objects = {}

    def get_model_info(self, model, id, level):
        from trytond.model import fields as tryton_fields
        key = (model, id)
        logger.info('get info for %s-%d', model, id)
        obj = self.pool.get(model).browse([id])[0]
        fields = obj.fields_get()

        self.objects[key] = {}
        for field in fields:

            if field in self.TRYTON_FIELDS:
                continue

            value = getattr(obj, field)
            if isinstance(obj._fields[field], tryton_fields.Function):
                continue
            elif fields[field]['type'] in ('many2one', 'one2one'):
                if not value:
                    continue

                field_id = value.id
                field_model = fields[field]['relation']
                self.add_object_to_export(0, field_model, field_id, level - 1)
                self.objects[key][field] = (field_model, field_id)

            elif fields[field]['type'] == 'one2many':
                field_model = fields[field]['relation']
                for field_id in value:
                    field_id = field_id.id
                    self.add_object_to_export(
                        None, field_model, field_id, level - 1)
            elif fields[field]['type'] == 'many2many':
                field_model = fields[field]['relation']
                m2m_model = obj._fields[field].relation_name
                M2MModel = self.pool.get(m2m_model)
                link_to_model = obj._fields[field].origin
                link_to_remote = obj._fields[field].target

                for field_id in value:
                    field_id = field_id.id
                    self.add_object_to_export(
                        None, field_model, field_id, level - 1)

                    m2mObj = M2MModel.search([
                        (link_to_model, '=', id),
                        (link_to_remote, '=', field_id)])
                    self.add_object_to_export(
                        None, m2m_model, m2mObj[0].id, level - 1)
            else:
                self.objects[key][field] = value

        return True

    def add_object_to_export(self, position, model, id, level):
        if level == 0:
            return None

        if (model, id) in self.objects_to_export:
            return False

        if position is not None:
            self.objects_to_export.insert(position, (model, id))
        else:
            self.objects_to_export.append((model, id))

        return self.get_model_info(model, id, level)

    def get_external_id(self, model, id):
        IrModelData = self.pool.get('ir.model.data')
        imd = IrModelData.search([('model', '=', model), ('db_id', '=', id)])
        if not imd:
            fs_id = f"{model.replace('.', '_')}_{id}"
            IrModelData.create([{
                'module': self.module,
                'model': model,
                'db_id': id,
                'fs_id': fs_id}])

            return fs_id

        return f'{imd[0].module}.{imd[0].fs_id}'

    def run(self):
        for id in self.options.ids:
            self.add_object_to_export(
                None, self.options.model, id, self.options.level)

        self.module = self.options.module
        if self.options.for_migrate:
            self.module += '_migrate'

        root = etree.Element('tryton')
        data = etree.SubElement(root, 'data')
        for obj in self.objects_to_export:
            model = obj[0]
            fields = self.objects[obj]
            external_id = self.get_external_id(*obj)
            record = etree.SubElement(data, 'record')
            record.set('model', model)
            record.set('id', external_id)
            for name, value in fields.items():
                field = etree.SubElement(record, 'field')
                field.set('name', name)
                if value is None:
                    value = ''

                if isinstance(value, bool):
                    field.set('eval', str(value))
                elif isinstance(value, tuple):
                    ref_external_id = self.get_external_id(*value)
                    field.set('ref', ref_external_id)
                else:
                    field.text = str(value)

        if self.objects_to_export:
            if self.options.commit:
                self.transaction.commit()

            with open('export.xml', 'bw') as fp:
                fp.write(etree.tostring(root, pretty_print=True))
