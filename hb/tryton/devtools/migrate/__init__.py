import os

import importlib.machinery
import importlib.util

from logging import getLogger

from ..base import CommandBase, define_parser_logging_arguments


logger = getLogger(__name__)


class Migrate(CommandBase):
    """Run initialiation or/and migration of the main project"""

    WITH_TRANSACTION = False

    @classmethod
    def define_parser_arguments(self, parser):
        super().define_parser_arguments(parser)
        define_parser_logging_arguments(parser)

    @classmethod
    def initialize_pool(self, pool):
        pass

    def run(self):
        from .session import Session

        if not os.path.isfile('migrate.py'):
            raise ImportError("No file 'migrate.py' in the current directory")

        loader = importlib.machinery.SourceFileLoader(
            'migrate', 'migrate.py')
        spec = importlib.util.spec_from_loader('migrate', loader)
        migrate = importlib.util.module_from_spec(spec)
        loader.exec_module(migrate)

        session = Session(self)
        migrate.migrate(session)
        session.update_previous_version_for_the_next_migration()


class CreateDB(CommandBase):
    """Create database"""

    WITH_TRANSACTION = False

    @classmethod
    def define_parser_arguments(self, parser):
        super().define_parser_arguments(parser)
        define_parser_logging_arguments(parser)
        parser.add_argument(
            '--modules',
            dest="modules",
            nargs='+',
            help='Specify the modules to install')

    @classmethod
    def initialize_pool(self, pool):
        pass

    def run(self):
        from .session import Session

        session = Session(self)
        session.migrate(*self.options.modules)
        session.update_previous_version_for_the_next_migration()
