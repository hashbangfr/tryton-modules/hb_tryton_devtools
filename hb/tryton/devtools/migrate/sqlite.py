from .backend import Backend
from trytond import backend


class SQLilteBackend(Backend):

    INIT_SQL_FILE = 'sqlite_init.sql'

    def exist(self):
        database = backend.Database().connect()
        return self.dbname in database.list()

    def create(self):
        database = backend.Database()
        database.connect()
        connection = database.get_connection(autocommit=True)
        try:
            database.create(connection, self.dbname)
        finally:
            database.put_connection(connection, True)

        return database

    def is_not_initialized(self):
        database = backend.Database()
        database.connect()
        connection = database.get_connection(autocommit=True)
        query = """
            SELECT name
            FROM sqlite_master
            WHERE type='table' AND name='hb_tryton_modules_version';
        """
        return connection.execute(query).fetchone() is None
