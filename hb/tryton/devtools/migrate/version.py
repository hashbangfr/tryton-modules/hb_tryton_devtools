from packaging import version


class TrytonVersion(version.Version):

    def parse_other(self, other):
        if other is None:
            other = version.Version('0.0.0')  # pragma: no cover
        elif not isinstance(other, version.Version):
            other = parse_version(other)

        return other

    def __lt__(self, other):
        other = self.parse_other(other)
        return super().__lt__(other)

    def __le__(self, other):
        other = self.parse_other(other)
        return super().__le__(other)

    def __eq__(self, other):
        other = self.parse_other(other)
        return super().__eq__(other)

    def __ne__(self, other):
        other = self.parse_other(other)
        return super().__ne__(other)

    def __ge__(self, other):
        other = self.parse_other(other)
        return super().__ge__(other)

    def __gt__(self, other):
        other = self.parse_other(other)
        return super().__gt__(other)


def parse_version(v):
    return TrytonVersion(v)
