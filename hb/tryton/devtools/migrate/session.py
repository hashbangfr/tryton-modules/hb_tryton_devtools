import os

import configparser

from logging import getLogger
from contextlib import contextmanager
from trytond import tools, __version__, convert, backend
from trytond.transaction import Transaction

from .version import parse_version


logger = getLogger(__name__)


class Session:

    def __str__(self):
        return f'<Session db={self.options.database_name}>'

    def __init__(self, migrate):
        self.options = migrate.options
        self.config = migrate.config
        self.pool = migrate.pool
        self.init = False

        backend_name = backend.name or 'postgresql'

        if backend_name == 'postgresql':
            from .postgresql import PostgresBackend
            self.database = PostgresBackend(
                self.options.database_name, self.config)
        elif backend_name == 'sqlite':
            from .sqlite import SQLilteBackend
            self.database = SQLilteBackend(
                self.options.database_name, self.config)

        if not self.database.exist():
            self.database.create()  # pragma: no cover

        self.init = self.database.initialize_tryton_database()

    def get_previous_version_for(self, module_name):
        with self.transaction() as transaction:
            with transaction.connection.cursor() as cursor:
                query = f"""
                    Select version
                    FROM hb_tryton_modules_version
                    WHERE module='{module_name}';"""
                logger.debug(query)
                cursor.execute(query)
                res = cursor.fetchone()
                if res is None:
                    return None

                return parse_version(res[0])

    def update_previous_version_for_the_next_migration(self):
        with self.transaction() as transaction:
            Module = self.pool.get('ir.module')
            with transaction.connection.cursor() as cursor:
                query = "DELETE FROM hb_tryton_modules_version;"
                logger.debug(query)
                cursor.execute(query)

                for module in Module.search([('state', '!=', 'not activated')]):
                    version = self.get_current_version_for(module.name)
                    if version:
                        query = f"""
                            INSERT INTO hb_tryton_modules_version
                            (module, version)
                            VALUES ('{module.name}', '{version}');"""
                        logger.debug(query)
                        cursor.execute(query)

    def get_current_version_for(self, module_name):
        if module_name in ('ir', 'res'):
            return __version__

        module_config = configparser.ConfigParser()
        with tools.file_open(os.path.join(module_name, 'tryton.cfg')) as fp:
            module_config.read_file(fp)

        info = dict(module_config.items('tryton'))
        return info.get('version', None)

    def migrate(self, *modules, **kwargs):
        lang = os.getenv(
            'TRYTON_LANG_CODE',
            self.config.get('database', 'language'))
        langs = [lang]
        if 'langs' in kwargs:
            langs.extend(kwargs['langs'])

        self.pool.init(
            update=list(modules),
            lang=langs,
            activatedeps=True)

        with self.transaction() as transaction:
            Module = self.pool.get('ir.module')
            Module.update_list()

            # Here we close the configuration
            Config = self.pool.get('ir.module.config_wizard.item')
            configs = Config.search([('state', '=', 'open')])
            Config.write(configs, {'state': 'done'})

            if self.init:

                # Load the lang
                Lang = self.pool.get('ir.lang')
                for lang_ in Lang.search([('code', 'in', langs)]):
                    lang_.translatable = True
                    lang_.save()

                # Put the default lang in the main configuration
                Configuration = self.pool.get('ir.configuration')
                configuration = Configuration(1)
                configuration.language = lang
                configuration.save()

                # Defined the mimum admin user
                with transaction.set_context(active_test=False):
                    User = self.pool.get('res.user')
                    admin, = User.search([('login', '=', 'admin')])
                    if self.get_current_version_for('ir') < '6.2.0':
                        admin.lang = lang  # pragma: no cover
                    else:
                        language = self.pool.get('ir.lang').search(
                            [('code', '=', lang)])[0]
                        admin.language = language

                    admin.password = os.getenv(
                        'TRYTON_ADMIN_PASSWORD', 'admin')
                    admin.save()

    @contextmanager
    def transaction(self):
        with Transaction().start(self.options.database_name, 0) as transaction:
            yield transaction

    @contextmanager
    def set_user(self, **kwargs):
        from hb.tryton.devtools.helper import user

        with user.set_user(**kwargs):
            yield

    def import_xml_file(self, module, xmlfile):
        lang = [self.config.get('database', 'language')]
        tryton_parser = convert.TrytondXmlHandler(
            self.pool, f'{module}_migrate', 'activated', [], lang)
        logger.info('loading %s', xmlfile)
        with tools.file_open(os.path.join(module, xmlfile), 'rb') as fp:
            tryton_parser.parse_xmlstream(fp)
