import os
import psycopg2
from contextlib import contextmanager
from .backend import Backend


class PostgresBackend(Backend):

    INIT_SQL_FILE = 'postgresql_init.sql'

    @contextmanager
    def cursor(self):
        uri = os.getenv(
            'TRYTON_DATABASE_URI',
            self.config.get('database', 'uri'))
        DSN = uri + '%s'
        dbname = False
        for db in (self.dbname, 'postgres', 'template0', 'template1', None):
            try:
                conn = psycopg2.connect(DSN % db or '')
                conn.close()
                dbname = db
                break
            except Exception:
                pass

        if dbname is False:
            raise Exception("Can't connect to postgres")  # pragma: no cover

        conn = None
        cur = None
        try:
            conn = psycopg2.connect(DSN % dbname or '')
            conn.set_isolation_level(
                psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            cur = conn.cursor()
            yield cur
        finally:
            if cur is not None:
                cur.close()
            if conn is not None:
                conn.close()

    def exist(self):
        query = f"SELECT 1 FROM pg_database WHERE datname='{self.dbname}';"
        with self.cursor() as cur:
            cur.execute(query)
            res = cur.fetchone()
            if res is None:
                return False  # pragma: no cover

            return True

        return False  # pragma: no cover

    def create(self):
        query = f"CREATE DATABASE {self.dbname}"
        with self.cursor() as cur:
            cur.execute(query)

        super().create()

    def is_not_initialized(self):
        with self.cursor() as cur:
            query = """
                SELECT EXISTS (
                    SELECT 1
                    FROM information_schema.tables
                    WHERE table_name = 'hb_tryton_modules_version'
                );
            """
            cur.execute(query)
            return not cur.fetchone()[0]
