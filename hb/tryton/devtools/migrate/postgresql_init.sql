CREATE TABLE hb_tryton_modules_version (
    module VARCHAR NOT NULL,
    version VARCHAR NOT NULL,
    PRIMARY KEY(module)
);
