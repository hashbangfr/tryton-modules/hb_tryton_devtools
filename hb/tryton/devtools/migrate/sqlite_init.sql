CREATE TABLE hb_tryton_modules_version (
    module VARCHAR PRIMARY KEY,
    version VARCHAR
);
