import os

from logging import getLogger
from trytond import backend
from trytond.transaction import Transaction


logger = getLogger(__name__)


class Backend:

    INIT_SQL_FILE = None

    def __init__(self, dbname, config):
        self.dbname = dbname
        self.config = config

    def exist(self):
        return False  # pragma: no cover

    def create(self):
        logger.info(f"The database '{self.dbname}' is created")

    def is_not_initialized(self):
        return False  # pragma: no cover

    def execute_sql_file(self):
        with Transaction().start(self.dbname, 0) as transaction:
            sql_file = os.path.join(
                os.path.dirname(__file__),
                self.INIT_SQL_FILE)
            with transaction.connection.cursor() as cursor:
                with open(sql_file, 'r') as fp:
                    cursor.execute(fp.read())

    def initialize_tryton_database(self):
        database = backend.Database(self.dbname)
        database.connect()
        if not database.test():
            database.init()
            self.execute_sql_file()
            logger.info('Initialize the database')
            return True

        if self.is_not_initialized():
            self.execute_sql_file()
            logger.info('Initialize the database')
            return True

        return False
