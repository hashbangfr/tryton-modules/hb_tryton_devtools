import sys

from code import InteractiveConsole
from trytond import __version__

from .base import CommandBase, define_parser_logging_arguments


class Interpreter(CommandBase):
    """Interactive console, work with Python and IPython"""

    @classmethod
    def define_parser_arguments(self, parser):
        super().define_parser_arguments(parser)
        define_parser_logging_arguments(parser)
        parser.add_argument(
            "--histsize",
            dest="histsize",
            type=int,
            default=500,
            help="The number of commands to remember in the command history")

    def run(self):
        # all options before load before import it
        local = {
            'pool': self.pool,
            'transaction': self.transaction,
            'options': self.options,
            'config': self.config,
        }
        banner2 = '\n'.join([
            '',
            '',
            'Available variables:',
            '',
            " * pool : Pool instance of the Tryton's models",
            " * transaction : Transaction instance with the admin user",
            " * options : configuration of the console script",
            " * config : configuration of Tryton",
            '',
            '',
            'Example of code',
            '',
            ">>> User = pool.get('res.user')",
            ">>> user = User.search(['login', '=', 'admin'])[0]",
            ">>> transaction.set_user(user)",
            '',
        ])

        try:
            from IPython import embed
            from IPython.core.usage import default_banner
            banner1 = default_banner + f'Tryton {__version__}'

            def func_(pool=None, transaction=None,
                      options=None, config=None):
                embed(banner1=banner1, banner2=banner2)

            func_(**local)
        except ImportError:
            if sys.stdin.isatty():
                from trytond.console import Console as TrytonConsole

                console = TrytonConsole(
                    local, histsize=self.options.histsize)
                banner = "Tryton %s, Python %s on %s \n%s" % (
                    __version__, sys.version, sys.platform, banner2)
                kwargs = {}
                if sys.version_info >= (3, 6):
                    kwargs['exitmsg'] = ''

                console.interact(banner=banner, **kwargs)
            else:
                console = InteractiveConsole(local)
                console.runcode(sys.stdin.read())
