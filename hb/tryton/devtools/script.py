from .base import CommandBase, define_parser_logging_arguments
import importlib


class Script(CommandBase):
    """Import and execute script"""

    @classmethod
    def define_parser_arguments(self, parser):
        super().define_parser_arguments(parser)
        define_parser_logging_arguments(parser)
        parser.add_argument(
            dest="python_script",
            metavar='FILE',
            help='Specify a python file to execute')

    def run(self):
        # all options before load before import it
        loader = importlib.machinery.SourceFileLoader(
            'script', self.options.python_script)
        spec = importlib.util.spec_from_loader('script', loader)
        script = importlib.util.module_from_spec(spec)

        script.pool = self.pool
        script.transaction = self.transaction
        script.options = self.options
        script.config = self.config

        loader.exec_module(script)
