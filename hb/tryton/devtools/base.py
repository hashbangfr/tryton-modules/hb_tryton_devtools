import os


def define_parser_logging_arguments(parser):
    parser.add_argument(
        "-v",
        "--verbose",
        action='count',
        dest="verbose",
        default=0,
        help="enable verbose mode")
    parser.add_argument(
        "--logconf",
        dest="logconf",
        metavar='FILE',
        help="logging configuration file (ConfigParser format)")


class CommandBase:
    "Explanation of the command"

    WITH_TRANSACTION = True

    def __init__(self, pool, transaction, options, config):
        self.pool = pool
        self.transaction = transaction
        self.options = options
        self.config = config

    @classmethod
    def define_parser_arguments(self, parser):
        parser.add_argument(
            "-c",
            "--config",
            dest="configfile",
            metavar='FILE',
            nargs='+',
            default=[os.environ.get('TRYTON_CONFIG')],
            help='Specify configuration files')
        parser.add_argument(
            "-d",
            "--database",
            dest="database_name",
            metavar='DATABASE',
            default=os.environ.get('TRYTON_DATABASE_NAME'),
            help="specify the database name")

    @classmethod
    def initialize_pool(self, pool):
        from trytond.transaction import Transaction

        with Transaction().start(pool.database_name, 0, readonly=True):
            pool.init()

    def run(self):  # pragma: no cover
        raise NotImplementedError('You must overwrite this method')

    def run_task(self):
        from trytond.worker import run_task
        while self.transaction.tasks:  # pragma: no cover
            task_id = self.transaction.tasks.pop()
            run_task(self.pool, task_id)
