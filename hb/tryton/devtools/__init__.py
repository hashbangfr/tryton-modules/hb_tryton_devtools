import os
from pkg_resources import iter_entry_points
from argparse import ArgumentParser
from trytond.config import config
from trytond import commandline


def get_commands():
    """Get the command class From the entry points"""
    res = {}
    for i in iter_entry_points('hb.tryton.devtools.command'):
        try:
            res[i.name] = i.load()
            print('Command %s is loaded' % i.name)
        except Exception as e:  # pragma: no cover
            print('Fail to load the command %s : %s' % (i.name, str(e)))

    return res


def get_parser(commands):
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(help="commands")

    for command, cls in commands.items():
        subparser = subparsers.add_parser(command, help=cls.__doc__)
        subparser.set_defaults(command=command)
        cls.define_parser_arguments(subparser)

    return parser


def HbTrytonAdmin():
    commands = get_commands()
    parser = get_parser(commands)

    options = parser.parse_args()
    config.update_etc(options.configfile)
    if (hasattr(options, 'logconf') and hasattr(options, 'verbose')):
        commandline.config_log(options)

    db_name = options.database_name
    if db_name is None:
        raise Exception('No database defined')

    Command = commands[options.command]

    database_uri = os.environ.get('TRYTON_DATABASE_URI')
    if database_uri:
        config.set('database', 'uri', database_uri)

    # import here, because the all configuration must be before
    from trytond.pool import Pool
    from trytond.transaction import Transaction
    pool = Pool(db_name)
    Command.initialize_pool(pool)

    if Command.WITH_TRANSACTION:
        with Transaction().start(db_name, 0) as transaction:
            command = Command(pool, transaction, options, config)
            command.run()
            transaction.rollback()

            command.run_task()
    else:
        command = Command(pool, None, options, config)
        command.run()
