from .base import CommandBase, define_parser_logging_arguments


class ExportLang(CommandBase):
    """Export module.pot or lang.po file"""

    @classmethod
    def define_parser_arguments(self, parser):
        super().define_parser_arguments(parser)
        define_parser_logging_arguments(parser)
        parser.add_argument(
            dest="module",
            help='Specify the name of the module to export')
        parser.add_argument(
            '--lang',
            dest="lang",
            help=(
                'Specify the name of the lang to export, '
                'if not filled the extension will be a pot'))

    def run(self):
        # all options before load before import it
        if self.options.lang:
            file_ = self.pool.get('ir.translation').translation_export(
                self.options.lang, self.options.module)
            file_name = f'{self.options.lang}.po'
        else:
            file_ = self.pool.get('ir.translation').translation_export(
                'en', self.options.module)
            file_name = f'{self.options.module}.pot'

        if file_:
            with open(file_name, 'wb') as fp:
                fp.write(file_ or b'')
