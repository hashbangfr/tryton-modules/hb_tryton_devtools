#!/usr/bin/env python3
import gettext
import pycountry
from forex_python.converter import CurrencyCodes
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.tools import remove_forbidden_chars
from logging import getLogger
from .common import get_language_codes


logger = getLogger(__name__)


def get_currencies():
    pool = Pool()
    Currency = pool.get('currency.currency')
    return {c.code: c for c in Currency.search([])}


def update_currencies(currencies):
    logger.info("Update currencies")
    pool = Pool()
    Currency = pool.get('currency.currency')
    codes = CurrencyCodes()

    records = []
    for currency in pycountry.currencies:
        code = currency.alpha_3
        if code in currencies:
            record = currencies[code]
        else:
            record = Currency(code=code)
        record.name = remove_forbidden_chars(currency.name)
        record.numeric_code = currency.numeric
        record.symbol = codes.get_symbol(currency.alpha_3) or currency.alpha_3
        records.append(record)

    Currency.save(records)
    return {c.code: c for c in records}


def translate_currencies(currencies):
    pool = Pool()
    Currency = pool.get('currency.currency')

    for code in get_language_codes():
        try:
            gnutranslation = gettext.translation(
                'iso4217', pycountry.LOCALES_DIR, languages=[code])
        except IOError:
            continue
        logger.info("Update currencies %s" % code)
        records = []
        for currency in pycountry.currencies:
            record = Currency(currencies[currency.alpha_3].id)
            record.name = remove_forbidden_chars(
                gnutranslation.gettext(currency.name))
            records.append(record)
        Currency.save(records)


def import_currencies():
    with Transaction().set_context(active_test=False):
        currencies = get_currencies()
        currencies = update_currencies(currencies)
        translate_currencies(currencies)
