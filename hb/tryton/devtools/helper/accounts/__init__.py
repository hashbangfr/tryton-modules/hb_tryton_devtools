from datetime import date
from dateutil.relativedelta import relativedelta
from trytond.pool import Pool
from .fr import import_accounts_fr  # noqa
from logging import getLogger


logger = getLogger(__name__)


def get_or_create_strict_seq(entry, company, **kwargs):
    pool = Pool()
    SequenceStrict = pool.get('ir.sequence.strict')
    if f'{entry}_id' in kwargs:
        return SequenceStrict.browse([kwargs[f'{entry}_id']])[0]

    value = kwargs.get(entry, {})
    if isinstance(value, dict):
        IrModelData = pool.get('ir.model.data')
        SequenceType = pool.get('ir.sequence.type')

        value['sequence_type'] = SequenceType.browse([IrModelData.get_id(
            'account_invoice', 'sequence_type_account_invoice')])[0]
        value['company'] = company
        if 'name' not in value:
            value['name'] = entry

        return SequenceStrict(**value)

    return value


def fiscal_year(company=None, start_date=None, interval=1, end_day=31,
                post_move_sequence=None, **kwargs):
    if start_date is None:
        start_date = date(date.today().year, 1, 1)

    end_date = start_date + relativedelta(years=+1, days=-1)

    if post_move_sequence is None:
        post_move_sequence = {}

    logger.info(f'Create fiscal year {start_date.year} for {company} with : '
                f'interval={interval}, end_day={end_day}, '
                f'post_move_sequence={post_move_sequence}')

    pool = Pool()
    FiscalYear = pool.get('account.fiscalyear')
    Sequence = pool.get('ir.sequence')
    SequenceType = pool.get('ir.sequence.type')
    IrModelData = pool.get('ir.model.data')
    sequence_type = SequenceType.browse([
        IrModelData.get_id('account', 'sequence_type_account_move')])[0]

    sequence = Sequence(
        name=f'Post move sequence {start_date.year}',
        sequence_type=sequence_type,
        company=company,
        **post_move_sequence)
    sequence.save()

    other = {}

    if 'account_invoice' in pool._modules:
        FiscalYearInvoiceSequences = pool.get(
            'account.fiscalyear.invoice_sequence')
        if 'invoice_sequence' in kwargs:
            seq = get_or_create_strict_seq(
                'invoice_sequence', company, **kwargs)
            in_credit_note_sequence = seq
            in_invoice_sequence = seq
            out_credit_note_sequence = seq
            out_invoice_sequence = seq
        else:
            in_credit_note_sequence = get_or_create_strict_seq(
                'in_credit_note_sequence', company, **kwargs)
            in_invoice_sequence = get_or_create_strict_seq(
                'in_invoice_sequence', company, **kwargs)
            out_credit_note_sequence = get_or_create_strict_seq(
                'out_credit_note_sequence', company, **kwargs)
            out_invoice_sequence = get_or_create_strict_seq(
                'out_invoice_sequence', company, **kwargs)

        other['invoice_sequences'] = [FiscalYearInvoiceSequences(
            company=company,
            in_credit_note_sequence=in_credit_note_sequence,
            in_invoice_sequence=in_invoice_sequence,
            out_credit_note_sequence=out_credit_note_sequence,
            out_invoice_sequence=out_invoice_sequence,
        )]

    fy = FiscalYear(
        name=kwargs.get('name', str(start_date.year)),
        start_date=start_date,
        end_date=end_date,
        post_move_sequence=sequence,
        company=company,
        **other)
    fy.save()
    FiscalYear.create_period([fy], interval=interval, end_day=end_day)
    return fy
