from trytond.pool import Pool
from .base import import_accounts


def import_accounts_fr(company_id):
    return import_accounts('account_fr.root', company_id, get_accounts_fr)


def get_accounts_fr(company):
    "Return accounts per kind"
    pool = Pool()
    Account = pool.get('account.account')

    accounts = {}
    accounts['receivable'], = Account.search([
            ('type.receivable', '=', True),
            ('company', '=', company.id),
            ('code', '=', '4111'),
            ], limit=1)
    accounts['payable'], = Account.search([
            ('type.payable', '=', True),
            ('company', '=', company.id),
            ('code', '=', '4011'),
            ], limit=1)
    accounts['revenue'], = Account.search([
            ('type.revenue', '=', True),
            ('company', '=', company.id),
            ('code', '=', '7011'),
            ], limit=1)
    accounts['expense'], = Account.search([
            ('type.expense', '=', True),
            ('company', '=', company.id),
            ('code', '=', '6071'),
            ], limit=1)
    accounts['cash'], = Account.search([
            ('company', '=', company.id),
            ('code', '=', '5311'),
            ])
    accounts['tax'], = Account.search([
            ('company', '=', company.id),
            ('code', '=', '44558'),
            ])
    return accounts
