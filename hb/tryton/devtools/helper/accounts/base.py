from trytond.pool import Pool
from logging import getLogger


logger = getLogger(__name__)


def get_instance_id(instance):
    if isinstance(instance, int):
        return instance

    pool = Pool()
    IrModelData = pool.get('ir.model.data')
    module, name = instance.split('.')
    return IrModelData.get_id(module, name)


def import_accounts(chart_id, company_id, get_accounts):
    logger.info('Import accounts')
    pool = Pool()
    chart_id = get_instance_id(chart_id)
    company_id = get_instance_id(company_id)

    AccountTemplate = pool.get('account.account.template')
    Company = pool.get('company.company')

    company = Company.browse([company_id])[0]
    account_template = AccountTemplate.browse([chart_id])[0]

    CreateChart = pool.get('account.create_chart', type='wizard')
    session, start, end = CreateChart.create()
    create_chart = CreateChart(session)
    create_chart._execute('account')
    create_chart.account.account_template = account_template
    create_chart.account.company = company
    create_chart._execute('create_account')

    accounts = get_accounts(company)

    create_chart._execute('properties')
    create_chart.properties.account_receivable = accounts['receivable']
    create_chart.properties.account_payable = accounts['payable']
    create_chart.properties.company = company
    create_chart._execute('create_properties')
    logger.info('Accounts are imported')
