#!/usr/bin/env python3
import gettext
import pycountry
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.tools import remove_forbidden_chars
from .common import get_language_codes
from logging import getLogger


logger = getLogger(__name__)


def get_countries():
    pool = Pool()
    Country = pool.get('country.country')
    return {c.code: c for c in Country.search([])}


def update_countries(countries):
    logger.info("Update countries")
    pool = Pool()
    Country = pool.get('country.country')

    records = []
    for country in pycountry.countries:
        code = country.alpha_2
        if code in countries:
            record = countries[code]
        else:
            record = Country(code=code)
        record.name = remove_forbidden_chars(country.name)
        record.code3 = country.alpha_3
        record.code_numeric = country.numeric
        records.append(record)

    Country.save(records)
    return {c.code: c for c in records}


def translate_countries(countries):
    pool = Pool()
    Country = pool.get('country.country')

    for code in get_language_codes():
        try:
            gnutranslation = gettext.translation(
                'iso3166', pycountry.LOCALES_DIR, languages=[code])
        except IOError:
            continue
        logger.info("Update countries %s" % code)
        records = []
        for country in pycountry.countries:
            record = Country(countries[country.alpha_2].id)
            record.name = remove_forbidden_chars(
                gnutranslation.gettext(country.name))
            records.append(record)
        Country.save(records)


def get_subdivisions():
    pool = Pool()
    Subdivision = pool.get('country.subdivision')
    return {(s.country.code, s.code): s for s in Subdivision.search([])}


def update_subdivisions(countries, subdivisions):
    logger.info("Update subdivisions")
    pool = Pool()
    Subdivision = pool.get('country.subdivision')

    records = []
    for subdivision in pycountry.subdivisions:
        code = subdivision.code
        country_code = subdivision.country_code
        if (country_code, code) in subdivisions:
            record = subdivisions[(country_code, code)]
        else:
            record = Subdivision(code=code, country=countries[country_code])
        record.name = remove_forbidden_chars(subdivision.name)
        record.type = subdivision.type.lower()
        records.append(record)

    Subdivision.save(records)
    return {(s.country.code, s.code): s for s in records}


def update_subdivisions_parent(subdivisions):
    logger.info("Update subdivisions parent")
    pool = Pool()
    Subdivision = pool.get('country.subdivision')

    records = []
    for subdivision in pycountry.subdivisions:
        code = subdivision.code
        country_code = subdivision.country_code
        record = subdivisions[(country_code, code)]
        if subdivision.parent:
            record.parent = subdivisions[
                (country_code, subdivision.parent.code)]
        else:
            record.parent = None
        records.append(record)
    Subdivision.save(records)


def translate_subdivisions(subdivisions):
    pool = Pool()
    Subdivision = pool.get('country.subdivision')

    for code in get_language_codes():
        try:
            gnutranslation = gettext.translation(
                'iso3166-2', pycountry.LOCALES_DIR, languages=[code])
        except IOError:
            continue
        logger.info("Update subdivisions %s" % code)
        records = []
        for subdivision in pycountry.subdivisions:
            record = Subdivision(subdivisions[
                    (subdivision.country_code, subdivision.code)].id)
            record.name = remove_forbidden_chars(
                gnutranslation.gettext(subdivision.name))
            records.append(record)
        Subdivision.save(records)


def import_countries():
    with Transaction().set_context(active_test=False):
        countries = get_countries()
        countries = update_countries(countries)
        translate_countries(countries)
        subdivisions = get_subdivisions()
        subdivisions = update_subdivisions(countries, subdivisions)
        update_subdivisions_parent(subdivisions)
        translate_subdivisions(subdivisions)
