from trytond.pool import Pool


def get_language_codes():
    pool = Pool()
    Language = pool.get('ir.lang')
    languages = Language.search([('translatable', '=', True)])
    for lang in languages:
        yield lang.code
