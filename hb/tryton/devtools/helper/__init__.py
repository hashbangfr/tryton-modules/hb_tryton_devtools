from logging import getLogger

logger = getLogger(__name__)


def run_task(transaction, pool, task_id):
    from trytond.status import processing
    from trytond import backend
    from trytond.exceptions import UserError

    Queue = pool.get('ir.queue')
    Error = pool.get('ir.error')
    name = '<Task %s@%s>' % (task_id, pool.database_name)
    logger.info('%s started', name)
    try:
        try:
            try:
                task, = Queue.search([('id', '=', task_id)])
            except ValueError:
                # the task was rollbacked, nothing to do
                return

            with processing(name):
                task.run()

        except backend.DatabaseOperationalError:
            raise
        except (UserError, UserWarning) as e:
            Error.log(task, e)
            raise
        logger.info('%s done', name)

    except Exception:
        logger.critical('%s failed', name, exc_info=True)
        raise


def run_back_tasks(transaction, pool, login='admin'):
    from .user import set_user

    with set_user(login=login):
        while transaction.tasks:
            task_id = transaction.tasks.pop()
            run_task(transaction, pool, task_id)
