from contextlib import contextmanager


@contextmanager
def set_user(login=None):
    from trytond.pool import Pool
    from trytond.transaction import Transaction
    pool = Pool()
    User = pool.get('res.user')

    user_id = 0
    user = None
    if login != 'root':
        try:
            user = User.search([('login', '=', login)])[0]
            user_id = user.id
        except Exception:
            raise KeyError(f'"{login}" is not a valid user')

    with Transaction().set_user(user_id):
        if user is None:
            with Transaction().reset_context():
                yield

        else:
            with Transaction().set_context(
                **User._get_preferences(user, context_only=True)
            ):
                yield user
