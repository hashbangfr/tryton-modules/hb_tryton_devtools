#!/usr/bin/env python3

import io
import os
from setuptools import setup, find_packages


def read(fname):
    content = io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()
    return content


setup(
    name='hb.tryton.devtools',
    version='0.1.0',
    description='Add some helper to develop tryton module',
    long_description=read('README.rst'),
    author='HashBang',
    author_email='contact@hashbang.fr',
    url='https://hashbang.fr/',
    keywords='tryton, devtools',
    packages=find_packages(),
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Framework :: Tryton',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: '
        'GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        ],
    license='GPL-3',
    python_requires='>=3.6',
    package_data={
        'hb': [
            'tryton/devtools/migrate/*.sql',
        ],
        'hb_devtools': [
            'tryton.cfg',
            'data/*.xml',
        ],
    },
    install_requires=[
        'trytond',
        'packaging',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'hb-tryton-admin=hb.tryton.devtools:HbTrytonAdmin',
        ],
        'hb.tryton.devtools.command': [
            'create-db=hb.tryton.devtools.migrate:CreateDB',
            'export-lang=hb.tryton.devtools.lang:ExportLang',
            'export-model=hb.tryton.devtools.model:ExportModel',
            'interpreter=hb.tryton.devtools.interpreter:Interpreter',
            'script=hb.tryton.devtools.script:Script',
            'migrate=hb.tryton.devtools.migrate:Migrate',
        ],
        'trytond.modules': [
            'hb_devtools=hb_devtools',
        ],
    },
)
