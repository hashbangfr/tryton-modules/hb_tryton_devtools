import pytest  # noqa
from hb.tryton.devtools.tests.testing import set_user


class TestUser:

    def test_default_user_is_admin(self, rollbacked_transaction, pool):
        User = pool.get('res.user')
        user = User.browse([rollbacked_transaction.user])[0]
        assert user.login == 'admin'

    def test_set_user_root(self, rollbacked_transaction, pool):
        with set_user('root'):
            assert rollbacked_transaction.user == 0
            assert rollbacked_transaction.context == {}

        assert rollbacked_transaction.context != {}

    def test_another_user(self, rollbacked_transaction, pool):
        User = pool.get('res.user')
        Lang = pool.get('ir.lang')
        user = User(
            login='test',
            language=Lang.search([('translatable', '=', True)])[0],
        )
        user.save()

        with set_user('test'):
            context = rollbacked_transaction.context.copy()
            assert rollbacked_transaction.user == user.id
            assert context['language'] == user.language.code

        assert rollbacked_transaction.context != context
